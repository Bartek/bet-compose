create table "match"(
	id serial primary key not null,
	start_date timestamp not null,
	first_team text not null,
	second_team text not null,
	ft_goal int4,
	st_goal int4,
	canceled bool not null default false,
	t1_win_course numeric(5, 2),
	t2_win_course numeric(5, 2),
	draw_course numeric(5, 2)
);

create table bet(
	id serial primary key not null,
	bet_value numeric(10, 2) not null,
	match_id int4 not null,
	course numeric(5, 2) not null,
	user_id int4 not null,
	bet_winner text not null,
	foreign key(match_id) references match(id)
);


create table user_funds(
	user_id int4 primary key not null,
	funds numeric(10, 2) not null,
	constraint user_funds_greater_zero check (funds >= 0)
);

