create table users(
    id serial not null primary key,
    login text not null,
    password_hash text not null,
    is_admin bool not null,
    email text not null
);

INSERT INTO public.users (login, password_hash, is_admin, email) VALUES('admin', '2757cb3cafc39af451abb2697be79b4ab61d63d74d85b0418629de8c26811b529f3f3780d0150063ff55a2beee74c4ec102a2a2731a1f1f7f10d473ad18a6a87', true, 'string@wp.pl');
